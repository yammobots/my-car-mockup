
// ***** Start - Search Range Slider of jQueryUI *****

$( "#mileage .slider-range-max" ).slider({
    range: true,
    min: 10,
    max: 10000,
    step: 10,
    values: [ 2000, 6000 ],
    slide: function( event, ui ) {
        $("#mileage .slider-range-max .ui-slider-handle:first .range-val").val(ui.values[0] + ' km');
        $("#mileage .slider-range-max .ui-slider-handle:last .range-val").val(ui.values[1] + ' km');
    }
});
var range_values = $( "#mileage .slider-range-max" ).slider("instance").options.values;
$("#mileage .slider-range-max .ui-slider-handle:first")
    .html("<input type='text' class='range-val' value='"+ range_values[0]  +" km'>");
$("#mileage .slider-range-max .ui-slider-handle:last")
    .html("<input type='text' class='range-val' value='"+ range_values[1] +" km'>");
$("#mileage .slider-range-max .ui-slider-handle .range-val").attr('readonly', 'true');

$( "#price .slider-range-max" ).slider({
    range: true,
    min: 50,
    max: 2000,
    step: 50,
    values: [ 200, 1200 ],
    slide: function( event, ui ) {
        $("#price .slider-range-max .ui-slider-handle:first .range-val").val(ui.values[0] + ' lakhs');
        $("#price .slider-range-max .ui-slider-handle:last .range-val").val(ui.values[1] + ' lakhs');
    }
});
var range_values = $( "#price .slider-range-max" ).slider("instance").options.values;
$("#price .slider-range-max .ui-slider-handle:first")
    .html("<input type='text' class='range-val' value='"+ range_values[0] +" lakhs'>");
$("#price .slider-range-max .ui-slider-handle:last")
    .html("<input type='text' class='range-val' value='"+ range_values[1] +" lakhs'>");
$("#price .slider-range-max .ui-slider-handle .range-val").attr('readonly', 'true');

// ***** End - Search Range Slider of jQueryUI *****


// ***** Start - Search Auto Complete of jQueryUI *****

var brand_vals = [
    "Brand 1",
    "AppleScript",
    "Asp",
    "BASIC",
    "C"
];
$( "#input_brand" ).autocomplete({
    source: brand_vals
});

var model_vals = [
    "Model 1",
    "AppleScript",
    "Asp",
    "BASIC",
    "C"
];
$( "#input_model" ).autocomplete({
    source: model_vals
});

var model_detail_vals = [
    "Model Detail 1",
    "AppleScript",
    "Asp",
    "BASIC",
    "C"
];
$( "#input_model_detail" ).autocomplete({
    source: model_detail_vals
});

// ***** End - Search Auto Complete of jQueryUI *****

